#!/usr/bin/env python3

from sys import exit
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='WTStatus App')
    parser.add_argument('--bind-address', '-b', type=str, default='127.0.0.1')
    parser.add_argument('--port', '-p', type=int, default=5000)
    parser.add_argument('--debug', '-d', action='store_true')
    parser.add_argument('--database-uri', '-db', type=str, default='sqlite:////tmp/wtstatus.db')
    parser.add_argument('--admin-token', '-a', type=str)
    parser.add_argument('command', choices=['run','createdb','createtestdata','deldb'])

    args = parser.parse_args()

    from wtstatus import app

    app.config['SQLALCHEMY_DATABASE_URI'] = args.database_uri

    if args.command == 'createdb':
        from wtstatus import manage
        manage.initDB()
        exit(0)
    if args.command == 'deldb':
        from wtstatus.models import db
        db.reflect()
        db.drop_all()
        exit(0)
    if args.command == 'createtestdata':
        from wtstatus import manage
        manage.createDummyData()
        exit(0)

    app.config['ADMIN_TOKEN'] = args.admin_token

    if args.debug:
        app.config['DEBUG'] = True

    app.run(
            host=args.bind_address,
            port=args.port)
