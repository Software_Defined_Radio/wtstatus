with import <nixpkgs> {}; {
  env = let
    uwsgip3 = pkgs.uwsgi.override {
    plugins = ["python3"];
  };
  in stdenv.mkDerivation {
    name = "wtstatus-env";
    buildInputs = [
      python3
      python3Packages.flask
      python3Packages.flask-restful
      python3Packages.flask_migrate
      python3Packages.flask_sqlalchemy
      python3Packages.sqlalchemy
      python3Packages.pytest
      uwsgip3
    ];
  };
}
