from flask import abort, Response
from functools import wraps

from wtstatus import models
from wtstatus import app

def getChallenge(challenge_id):
    challenge = models.Challenge.query.filter_by(id=challenge_id)
    return challenge.first_or_404()

def getChallenges():
    challenges = list(models.Challenge.query.all())
    return challenges

def getChallengesDict():
    challenges = getChallenges()
    ret = {}
    for challenge in challenges:
        ret[challenge.id] = {"title": challenge.title, "active": challenge.active, "sending": challenge.sending}
    return ret

def getStatusOverview():
    challenges = getChallenges()
    ret = [c.dataDict for c in challenges]
    return ret

def getDevice(device_id):
    device = models.Device.query.filter_by(id=device_id)
    return device.first_or_404()

def getToken(device_id):
    device = models.Device.query.filter_by(id=device_id).first_or_404()
    token = models.Token.query.filter_by(challenge_id=device.challenge_id).first_or_404()
    return token

def getTokenByID(token_id):
    token = models.Token.query.filter_by(id=token_id).first_or_404()
    return token

def getTokens():
    tokens = list(models.Token.query.all())
    return tokens

def isTokenValid(tok,device_id):
    device = getDevice(device_id)
    tokens = models.Token.query.filter_by(challenge_id=device.challenge_id,token=tok).one_or_none()
    return tokens!=None

def getAlerts():
    devices = list(models.Device.query.all())
    ret = {}
    for device in devices:
        if device.isAlert:
            challenge = models.Challenge.query.filter_by(id=device.challenge_id).one_or_none()
            ret[device.id] = {"challenge-titel":challenge.title,"title": device.name,"last-active-beacon":device.last_active_beacon }
    return ret

def createChallenge(title):
    challenge = models.Challenge(title=title)
    models.db.session.add(challenge)
    models.db.session.commit()
    return challenge

def createDevice(name, challenge_id, timeout=None, sending_timeout=None):
    device = models.Device(name=name, challenge_id = challenge_id, timeout=timeout, sending_timeout=sending_timeout)
    models.db.session.add(device)
    models.db.session.commit()
    return device

def createToken(challenge_id):
    token = models.Token(challenge_id = challenge_id)
    models.db.session.add(token)
    models.db.session.commit()
    return token

def delete(obj):
    models.db.session.delete(obj)
    models.db.session.commit()

def isAdmin(admin_token):
    if not app.config['ADMIN_TOKEN'] or app.config['ADMIN_TOKEN'] != admin_token:
        return False
    return True

def adminWrapper(f):
    "Wrapped functions need to get the token as first positional argument"
    @wraps(f)
    def privilegeChecker(*args, **kwargs):
        if not isAdmin(args[0]):
            abort(401)
        return f(*args, **kwargs)
    return privilegeChecker

@adminWrapper
def adminAddChallenge(token, title):
    challenge = list(models.Challenge.query.filter_by(title=title))
    if challenge:
        abort(Response("Challenge exists", 409))
    return createChallenge(title)

@adminWrapper
def adminAddDevice(token, name, timeout, sending_timeout, challenge_id):
    challenge = list(models.Challenge.query.filter_by(id=challenge_id))
    if not challenge:
        abort(Response("Challenge does not exist", 400))
    return createDevice(name, challenge_id, timeout, sending_timeout)

@adminWrapper
def adminRemoveDevice(token, device_id):
    device = getDevice(device_id)
    delete(device)

@adminWrapper
def adminRemoveChallenge(token, challenge_id):
    challenge = getChallenge(challenge_id)
    delete(challenge)

@adminWrapper
def adminGetTokens(token):
    return [token.serialize for token in getTokens()]

@adminWrapper
def adminAddToken(token, challenge_id):
    token = createToken(challenge_id)
    return token

@adminWrapper
def adminRemoveToken(token, token_id):
    token = getTokenByID(token_id)
    delete(token)
