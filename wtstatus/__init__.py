from flask import Flask

from wtstatus.app_builder import genApp
app = genApp()

import wtstatus.views
import wtstatus.rest
import wtstatus.manage
