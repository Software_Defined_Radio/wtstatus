from wtstatus import models
from wtstatus import utils

def initDB():
    models.db.create_all()

def dropDB():
    models.db.session.remove()
    models.db.drop_all()

def createDummyData(challenge_name = "TestChallenge", device_timeouts = [None, 60, 10]):
    challenge = utils.createChallenge(challenge_name)
    for i in range(1,4):
        utils.createDevice("Device %d" % i, challenge.id, device_timeouts[i-1])
    utils.createToken(challenge.id)
    return challenge.id
