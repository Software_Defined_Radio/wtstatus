from flask import jsonify,abort
from flask_restful import Resource, Api, fields, marshal_with, reqparse

from wtstatus import app
from wtstatus import utils

api = Api(app)

challenge_fields = {
    'title': fields.String,
    'active': fields.Boolean,
    'sending': fields.Boolean,
    'device_list': fields.String,
}

device_fields = {
    'name': fields.String,
    'active': fields.Boolean,
    'sending': fields.Boolean,
    'note': fields.String,
}

class Challenge(Resource):
    @marshal_with(challenge_fields)
    def get(self, challenge_id):
        challenge = utils.getChallenge(challenge_id)
        return challenge

class Challenges(Resource):
    def get(self):
        return utils.getChallengesDict()

class Device(Resource):
    @marshal_with(device_fields)
    def get(self, device_id):
        return utils.getDevice(device_id)

    @marshal_with(device_fields)
    def post(self, device_id):
        device = utils.getDevice(device_id)
        args = device_parser.parse_args()
        print("queryied token{}:".format(args['token']))
        if utils.isTokenValid(args['token'],device_id):
            if args['active'] is not None:
                active = True if args['active'] == 'true' else False
                device.active = active
            if args['note'] is not None:
                device.note = args['note']
            if args['sending'] is not None:
                sending = True if args['sending'] == 'true' else False
                device.sending = sending
        else:
            abort(401)
        return device


class Alerts(Resource):
    def get(self):
        return jsonify(utils.getAlerts())

device_parser = reqparse.RequestParser()
device_parser.add_argument('active', choices=("true", "false"))
device_parser.add_argument('sending', choices=("true", "false"))
device_parser.add_argument('note', type=str)
device_parser.add_argument('token', type=str)

class ChallengesAdmin(Resource):
    def post(self):
        args = challenges_admin_parser.parse_args()
        challenge = utils.adminAddChallenge(args['admin_token'], args['title'])
        dummy = challenge.id
        return jsonify(challenge.serialize)

challenges_admin_parser = reqparse.RequestParser()
challenges_admin_parser.add_argument('title', type=str, required=True)
challenges_admin_parser.add_argument('admin_token', type=str, required=True)

class ChallengeAdmin(Resource):
    def delete(self, challenge_id):
        args = admin_parser.parse_args()
        utils.adminRemoveChallenge(args['admin_token'], challenge_id)
        return "ok"

class DevicesAdmin(Resource):
    def post(self):
        args = devices_admin_parser.parse_args()
        device = utils.adminAddDevice(args['admin_token'], args['name'], args['timeout'], args['sending_timeout'], args['challenge_id'])
        dummy = device.id # without this, the serialization will fail at this point
        return jsonify(device.serialize)

devices_admin_parser = reqparse.RequestParser()
devices_admin_parser.add_argument('name', type=str, required=True)
devices_admin_parser.add_argument('admin_token', type=str, required=True)
devices_admin_parser.add_argument('challenge_id', type=int, required=True)
devices_admin_parser.add_argument('timeout', type=float)
devices_admin_parser.add_argument('sending_timeout', type=float)


class DeviceAdmin(Resource):
    def get(self, device_id):
        args, device = self.common(device_id)
        return jsonify(device.serialize)

    def delete(self, device_id):
        args = admin_parser.parse_args()
        utils.adminRemoveDevice(args['admin_token'], device_id)
        return "ok"

    def common(self, device_id):
        args = admin_parser.parse_args()
        if not utils.isAdmin(args['admin_token']):
            abort(401)
        device = utils.getDevice(device_id)
        return args, device

class TokensAdmin(Resource):
    def get(self):
        args = admin_parser.parse_args()
        return utils.adminGetTokens(args['admin_token'])

    def post(self):
        args = tokens_admin_parser.parse_args()
        token = utils.adminAddToken(args['admin_token'], args['challenge_id'])
        dummy = token.id # without this, the serialization will fail at this point
        return jsonify(token.serialize)

    def delete(self):
        args = tokens_delete_admin_parser.parse_args()
        utils.adminRemoveToken(args['admin_token'], args['token_id'])
        return "ok"

tokens_admin_parser = reqparse.RequestParser()
tokens_admin_parser.add_argument('admin_token', type=str, required=True)
tokens_admin_parser.add_argument('challenge_id', type=int, required=True)

tokens_delete_admin_parser = reqparse.RequestParser()
tokens_delete_admin_parser.add_argument('admin_token', type=str, required=True)
tokens_delete_admin_parser.add_argument('token_id', type=int, required=True)

admin_parser = reqparse.RequestParser()
admin_parser.add_argument('admin_token', type=str, required=True)
api.add_resource(Alerts,'/api/alerts', endpoint='alerts')
api.add_resource(Challenges,'/api/challenges', endpoint='challenges')
api.add_resource(Challenge,'/api/challenges/<int:challenge_id>', endpoint='challenge')
api.add_resource(Device,'/api/devices/<int:device_id>', endpoint='device')
api.add_resource(ChallengesAdmin,'/api/admin/challenges', endpoint='challengesAdmin')
api.add_resource(ChallengeAdmin,'/api/admin/challenges/<int:challenge_id>', endpoint='challengeAdmin')
api.add_resource(DevicesAdmin,'/api/admin/devices', endpoint='devicesAdmin')
api.add_resource(DeviceAdmin,'/api/admin/devices/<int:device_id>', endpoint='deviceAdmin')
api.add_resource(TokensAdmin,'/api/admin/tokens', endpoint='tokensAdmin')
