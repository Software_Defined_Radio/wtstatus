from flask import render_template

from wtstatus import app, utils

@app.route('/')
def index():
    status = utils.getStatusOverview()
    return render_template('status.html', status=status)
