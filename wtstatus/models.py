from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime, timedelta

from wtstatus import app
import uuid

db = SQLAlchemy(app)
migrate = Migrate(app, db)

def token_gen():
    token = uuid.uuid4().hex
    print("token:{}".format(token))
    return token

def serializeModel(obj):
    r = {}
    for key, value in vars(obj).items():
        if not key.startswith('_'):
            r[key] = value
    return r

class Challenge(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), unique=True, nullable=False)

    def __repr__(self):
        return '<Challenge %s>' % self.title

    @property
    def serialize(self):
        return serializeModel(self)

    @property
    def active(self):
        return self.devices and all([d.active for d in self.devices])
    
    @property
    def sending(self):
        return self.devices and all([d.sending for d in self.devices])

    @property
    def dataDict(self):
        data = {"title": self.title, "active": self.active, "sending": self.sending}
        data["devices"] = [{"name": d.name, "active": d.active, "note": d.note, "sending": d.sending} for d in self.devices]
        return data

    @property
    def device_list(self):
        return ",".join([str(device.id) for device in self.devices])

class Token(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(32), unique=True, nullable=False,default=token_gen)
    challenge_id = db.Column(db.Integer, db.ForeignKey('challenge.id'), nullable=False)
    challenge = db.relationship('Challenge', backref=db.backref('token',cascade="all,delete", lazy=True))

    @property
    def serialize(self):
        return serializeModel(self)

class Device(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)

    challenge_id = db.Column(db.Integer, db.ForeignKey('challenge.id'), nullable=False)
    challenge = db.relationship('Challenge', backref=db.backref('devices', cascade="all,delete", lazy=True))

    last_active_beacon = db.Column(db.DateTime, nullable=True, default=None)
    last_active = db.Column(db.Boolean, default=False)
    last_sending = db.Column(db.Boolean, default=False)
    last_sending_beacon = db.Column(db.DateTime, nullable=True, default=None)
    last_note = db.Column(db.String(120), nullable=True, default=None)
    timeout = db.Column(db.Integer, nullable=True, default=None)
    sending_timeout = db.Column(db.Integer, nullable=True, default=None)
    activity_timeout = db.Column(db.Integer,nullable=True,default=120)

    @property
    def serialize(self):
        return serializeModel(self)

    def __repr__(self):
        return '<Device %s>' % self.name

    @property
    def isAlert(self):
        if not self.activity_timeout:
            return False
        if not self.last_active_beacon:
            return True
        td = timedelta(seconds=self.activity_timeout)
        if td + self.last_active_beacon < datetime.now():
            return True
        return False

    def isLastValid(self, last_beacon, timeout):
        if not timeout:
            return True
        if not last_beacon:
            return False
        td = timedelta(seconds=timeout)
        if last_beacon + td > datetime.now():
            return True
        return False

    def isLastActiveValid(self):
        return self.isLastValid(self.last_active_beacon, self.timeout)

    def isLastSendingValid(self):
        return self.isLastValid(self.last_sending_beacon, self.sending_timeout)

    @property
    def active(self):
        return (self.last_active if self.isLastActiveValid() else False)

    @property
    def sending(self):
        return (self.active and (self.last_sending if self.isLastSendingValid() else False))

    @active.setter
    def active(self, value):
        self.last_active = value
        self.last_active_beacon = datetime.now()
        db.session.commit()

    @sending.setter
    def sending(self, value):
        self.last_sending = value
        self.last_sending_beacon = datetime.now()
        db.session.commit()

    @property
    def note(self):
        return (self.last_note if self.active else None)

    @note.setter
    def note(self, value):
        self.last_note = value
        db.session.commit()
