import os
from flask import Flask

envPrefix = "WTSTATUS_"
envOptions = ["DEBUG", "SQLALCHEMY_DATABASE_URI", "ADMIN_TOKEN"]

def parseEnv(app, envPrefix = envPrefix, envOptions = envOptions):
    for opt in envOptions:
        value = os.environ.get(envPrefix + opt)
        if value != None:
            if value == "True":
                value = True
            if value == "False":
                value = False
            app.config[opt] = value

def genApp():
    app = Flask(__name__, instance_relative_config=True)

    try:
        app.config.from_object('config')
    except ModuleNotFoundError:
        pass
    try:
        app.config.from_pyfile('config.py')
    except FileNotFoundError:
        pass
    try:
        app.config.from_envvar('APP_CONFIG_FILE')
    except RuntimeError:
        pass

    parseEnv(app)

    return app
