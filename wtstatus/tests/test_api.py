import os
import tempfile

from functools import wraps
from time import sleep

import pytest

import wtstatus as wts

def challengeWrapper(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        c = kwargs['client']
        c.challenge = c.get('/api/challenges/%s' % c.challenge_id).get_json()
        f(*args, **kwargs)
    return decorated_function

def updateChallenge(c):
    c.challenge = c.get('/api/challenges/%s' % c.challenge_id).get_json()

def updateDevices(c):
    c.devices = {}
    devices = c.challenge['device_list'].split(',')
    for device in devices:
        c.devices[device] = c.get('/api/devices/%s' % device).get_json()

def update(c):
    updateChallenge(c)
    updateDevices(c)

def devicesWrapper(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        c = kwargs['client']
        updateDevices(c)
        f(*args, **kwargs)
    return decorated_function

@pytest.fixture(scope="module")
def client():
    wts.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    wts.app.config['TESTING'] = True
    wts.app.config['ADMIN_TOKEN'] = 'dummy'
    client = wts.app.test_client()

    with wts.app.app_context():
        wts.manage.initDB()

    return client

def test_no_challenge_exists(client):
    challenges = client.get('/api/challenges').get_json()
    assert challenges == {}

def test_unauthorized_add_challenge(client):
    status = client.post('/api/admin/challenges', data = dict(
        title = "TestChallenge",
        admin_token = "wrong",
    )).status_code
    assert status == 401

def test_add_challenge(client):
    challenge = client.post('/api/admin/challenges', data = dict(
        title = "TestChallenge",
        admin_token = "dummy",
    )).get_json()
    client.challenge_id = challenge['id']
    assert client.challenge_id

def test_challenge_exists(client):
    challenges = client.get('/api/challenges').get_json()
    assert str(client.challenge_id) in challenges
    assert challenges[str(client.challenge_id)]["title"] == "TestChallenge"

@challengeWrapper
def test_unauthorized_add_token(client):
    status = client.post('/api/admin/tokens', data = dict(
        admin_token = "wrong",
        challenge_id = client.challenge_id
    )).status_code
    assert status == 401

@challengeWrapper
def test_add_token(client):
    token = client.post('/api/admin/tokens', data = dict(
        admin_token = "dummy",
        challenge_id = client.challenge_id
    )).get_json()
    client.token_id = token['id']
    client.token = token['token']
    tokens = client.get('/api/admin/tokens', data = dict(
        admin_token = "dummy"
    )).get_json()
    assert tokens[0]['token'] == client.token

@challengeWrapper
def test_challenge_inactive(client):
    assert client.challenge['active'] == False

@challengeWrapper
def test_unauthorized_add_device(client):
    status = client.post('/api/admin/devices', data = dict(
        admin_token = "wrong",
        name = "TestDeviceStatic",
        challenge_id = client.challenge_id
    )).status_code
    assert status == 401

@challengeWrapper
def test_add_static_device(client):
    device = client.post('/api/admin/devices', data = dict(
        admin_token = "dummy",
        name = "TestDeviceStatic",
        challenge_id = client.challenge_id
    )).get_json()
    client.static_device_id = device['id']
    assert client.static_device_id

@challengeWrapper
def test_add_dynamic_device(client):
    device = client.post('/api/admin/devices', data = dict(
        admin_token = "dummy",
        name = "TestDeviceDynamic",
        challenge_id = client.challenge_id,
        timeout = 2.3
    )).get_json()
    client.dynamic_device_id = device['id']
    assert client.dynamic_device_id

@challengeWrapper
@devicesWrapper
def test_devices_inactive(client):
    for device, data in client.devices.items():
        assert data['active'] == False

@challengeWrapper
@devicesWrapper
def test_static_device(client):
    n = "I'm active"
    client.post('/api/devices/%s' % client.static_device_id, data = dict(
        active = "true",
        note = n,
        token = client.token
    ))
    update(client)
    assert client.devices[str(client.static_device_id)]['active'] == True
    assert client.devices[str(client.static_device_id)]['note'] == n

@challengeWrapper
@devicesWrapper
def test_timeout_device(client):
    n = "I'm active"
    d = str(client.dynamic_device_id)
    client.post('/api/devices/%s' % d, data = dict(
        active = "true",
        note = n,
        token = client.token
    ))
    update(client)
    assert client.devices[d]['active'] == True
    assert client.devices[d]['note'] == n
    sleep(2.3)
    update(client)
    assert client.devices[d]['active'] == False
    assert client.devices[d]['note'] == None

def test_unauthorized_delete_device(client):
    status = client.delete('/api/admin/devices/%s' % client.static_device_id, data = dict(
        admin_token = "wrong",
    )).status_code
    assert status == 401

@challengeWrapper
@devicesWrapper
def test_delete_device(client):
    status = client.delete('/api/admin/devices/%s' % client.static_device_id, data = dict(
        admin_token = "dummy",
    )).status_code
    update(client)
    assert status == 200
    assert str(client.static_device_id) not in client.devices

def test_unauthorized_delete_token(client):
    status = client.delete('/api/admin/tokens', data = dict(
        admin_token = "wrong",
        token_id = client.token_id
    )).status_code
    assert status == 401

def test_delete_token(client):
    status = client.delete('/api/admin/tokens', data = dict(
        admin_token = "dummy",
        token_id = client.token_id
    )).status_code
    assert status == 200
    tokens = client.get('/api/admin/tokens', data = dict(
        admin_token = "dummy"
    )).get_json()
    assert tokens == []

def test_unauthorized_delete_challenge(client):
    status = client.delete('/api/admin/challenges/%s' % client.challenge_id, data = dict(
        admin_token = "wrong",
    )).status_code
    assert status == 401

@challengeWrapper
@devicesWrapper
def test_delete_challenge(client):
    status = client.delete('/api/admin/challenges/%s' % client.challenge_id, data = dict(
        admin_token = "dummy",
    )).status_code
    assert status == 200
    status = client.get('/api/challenges/%s' % client.challenge_id).status_code
    assert status == 404
